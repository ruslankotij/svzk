(function(){
   let me = {};
    me.toggleClass = function () {

    const nav = document.querySelector('.nav');
    nav.addEventListener('click', function (e) {
        const navItems = document.querySelectorAll('.nav__item');
        for (let i=0;i<navItems.length;i++) {
            navItems[i].classList.remove('nav--active');
        }
        let target = e.target;
        if(e.target.tagName!=='A') return;
        target.classList.add('nav--active');
        me.showSection(target);
    });
    };

    me.showSection = function (e) {
        let dataLink = e.dataset.link;
        let section = document.querySelector('.' + dataLink);
        let coords = section.getBoundingClientRect();
        let top = coords.top;
            let timer = setInterval(function () {
                if (document.body.scrollTop < top) {
                    window.scrollBy(0, 10);
                    top -= 10;

                } else {
                    clearInterval(timer);

                }

            }, 10);
    };
    RUS.scrollingSections = me;
    RUS.scrollingSections.toggleClass();
}());