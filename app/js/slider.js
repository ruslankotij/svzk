(function() {

    let me = {};
    me.slick = function () {
        $('.slider__container').slick({
            appendArrows: '.arrow',
            appendDots: '.dots',
            prevArrow: '<button type="button" class="arrow__button arrow__left"><i class="page__prev fas' +
            ' fa-chevron-left"></i></button>',
            nextArrow: '<button type="button" class="arrow__button arrow__right"><i class="page__next fas' +
            ' fa-chevron-right"></i></button>',
            dots: true
            // dotsClass: 'dots'
        });
        $('.project__list').slick({
            appendArrows: '.arrows',
            prevArrow: '<button type="button" class="project__arrow project__prev">Предыдущий<br>проект<i' +
            ' class="page__prev fas' +
            ' fa-chevron-left"></i></button>',
            nextArrow: '<button type="button" class="project__arrow project__next">Следующий<br>проект<i' +
            ' class="page__next fas' +
            ' fa-chevron-right"></i></button>'
            // dots: true,
            // dotsClass: 'dots'
        });

        // $('.comments__list').slick({
        //     appendArrows: '.comments__arrows',
        //     prevArrow: '<button type="button" class="comments__arrow comments__arrow-left"></button>',
        //     nextArrow: '<button type="button" class=" comments__arrow comments__arrow-right"></button>',
        //     dots: true
        // });
    };

    $('.comments__list').owlCarousel({
            items: 1,
            nav: true,
            loop: true,
            navContainerClass: 'comments__arrows',
        });


var owlProjects = $(".hero__list");
    function setSlider(){
        if($(window).width() < 860){
            owlProjects.owlCarousel({
            items: 1,
            nav: true,
            loop: true,
            dotsClass: 'hero__dots',
            navContainerClass: 'hero__arrow'
        });
        }else {
            owlProjects.trigger('destroy.owl.carousel');

        }
    }
    setSlider();
    $(window).resize(setSlider);

RUS.slider = me;
RUS.slider.slick();
}());
